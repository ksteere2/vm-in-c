#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include "vm.h"
#include "rom.h"
#include "windows.h"

/* Global Variables */
uint16_t RAM[RAMSIZE];

uint16_t instruction=0, Areg=0, Dreg=0, ALUout=0,toALU=0, toAreg=0, pc=0; //16 bit variables
uint8_t instr; //8 bit variable
int i, ignore1, ignore2, a, c1, c2, c3, c4, c5, c6, d1, d2, d3, j1, j2, j3; //The bits
int jump=0; //jump yes no.

/* initialize_vm()
 * Zeros all storage locations.
 */
int initialize_vm () {
  /* Set the program counter, A, and D registers to zero. */
  /* Zero out RAM. */
  Areg = 0;
  Dreg = 0;
  int i=0;
  for (i = 0 ; i < RAMSIZE ; i++) {
    RAM[i] = 0;
  }

  /* Return non-zero in case of error. */
  return 0;
}
uint16_t alu(uint16_t x, uint16_t y, uint8_t instr) {

    printf("\nx = %d, y = %d, instr = %d,", x, y, instr);
    switch(instr){
        case 0x2A:
            return 0;
            break;
        case 0x3F:
            return 1;
            break;
        case 0x3A:
            return -1;
            break;
        case 0x0C:
            return x;
            break;
        case 0x30:
            return y;
            break;
        case 0x0D:
            return !x;
            break;
        case 0x31:
            return !y;
            break;
        case 0x0F:
            return -x;
            break;
        case 0x33:
            return -y;
            break;
        case 0x1F:
            return x+1;
            break;
        case 0x37:
            return y+1;
            break;
        case 0x0E:
            return x-1;
            break;
        case 0x32:
            return y-1;
            break;
        case 0x02:
            return x+y;
            break;
        case 0x07:
            return y-x;
            break;
        case 0x0:
            return x&y;
            break;
        case 0x15:
            return x|y;
            break;
        }
}
/* print_vm()
 * This is a handy debugging tool. It prints the A and D registers,
 * as well as the PC, and RAM. Your variable names may need to change.
 *
*/
void print_vm () {
  printf("\n");
  printf(  "Areg     %04x ", Areg);
  p(sizeof(uint16_t), &Areg);
  printf(  "Dreg     %04x ", Dreg);
  p(sizeof(uint16_t), &Dreg);
  printf(  "pc       %04x ", pc);
  p(sizeof(uint16_t), &pc);

  int ndx = 0;
  for (ndx = 0; ndx < RAMSIZE; ndx++) {
    printf("RAM [%04X] %04x ", ndx, RAM[ndx]);
    p(sizeof(uint16_t), &RAM[ndx]);
  }
}

int runloop () {

  while (pc <= 6)
  {
    //Get me an instruction
    instruction = ROM[pc];

    //Decode it
    i = ((instruction & 0x8000) >> 15);
    a = ((instruction & 0x1000) >> 12);
    c1 = ((instruction & 0x0800) >> 11);
    c2 = ((instruction & 0x0400) >> 10);
    c3 = ((instruction & 0x0200) >> 9);
    c4 = ((instruction & 0x0100) >> 8);
    c5 = ((instruction & 0x0080) >> 7);
    c6 = ((instruction & 0x0040) >> 6);
    d1 = ((instruction & 0x0020) >> 5);
    d2 = ((instruction & 0x0010) >> 4);
    d3 = ((instruction & 0x0008) >> 3);
    j1 = ((instruction & 0x0004) >> 2);
    j2 = ((instruction & 0x0002) >> 1);
    j3 = ((instruction & 0x0001) >> 0);
    instr = ((instruction >> 6) & 0x003F); //computation bits for ALU

    //Prints the instructinal bits
    printf("\nthe bits: i=%d, a=%d, d1=%d, d2=%d, d3=%d, j1=%d, j2=%d, j3=%d", i, a, d1, d2, d3, j1, j2, j3);

    //A register (first Mux)---------------
    if(i){
        toAreg = ALUout;
    }
    else{
        toAreg = instruction;
    }

    //Areg---------------------------------
    if((i && d1) || !i){
        Areg = toAreg;
    }

    //ALU inputs (second Mux)--------------
    if(i && a){
        toALU = RAM[Areg];
    }
    else{
        toALU = Areg;
    }

    ALUout = alu(Dreg, toALU, instr);

    //writeM----------------------------------
    if(i && d3){
        RAM[Areg] = ALUout;
    }

    //D register------------------------------
    if(i && d2){
        Dreg = ALUout;
    }

    //Jump condictions------------------------
    jump = 0;
    if((ALUout > 0) && (j3)){jump = 1;} // JGT
    if((ALUout = 0) && (j2)){jump = 1;} //JEQ
    if((ALUout >= 0)&& (j2 && j3)){jump = 1;} //JGE
    if((ALUout < 0)&& (j1)){jump = 1;} //JLT
    if((ALUout != 0)&& (j1 && j2)){jump = 1;} //JNE
    if((j1 && j2) && j3){jump = 1;} //JMP
    else{jump = 0;}

    if(jump && i){
        ROM[pc] = Areg;
    }
    else{
        pc = pc + 1;
    }

    //Pausing and print every instruction helps you figure out what is going on.
    print_vm();
    printf("\n");
    Sleep(1000);
  }
  return 0;
}

int main (int argv, char *args[]) {
  initialize_vm ();
  print_vm ();
  runloop();

  return 0;
}
